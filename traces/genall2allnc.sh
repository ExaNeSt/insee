
nodes=$(($1 - 1))

RANGE=1000
SEED=1

RANDOM=$SEED

for i in `seq 0 $nodes`; do
    for j in `seq 0 $nodes`; do
        number=$RANDOM
        let "number %= $RANGE"
        if [ "$i" -ne "$j" ]; then
            echo s $i $j 14 $number;
        fi
    done
done

RANDOM=$SEED

for i in `seq 0 $nodes`; do
    for j in `seq 0 $nodes`; do
        number=$RANDOM
        let "number %= $RANGE"
        if [ "$i" -ne "$j" ]; then
            echo r $i $j 14 $number;
        fi
    done
done
